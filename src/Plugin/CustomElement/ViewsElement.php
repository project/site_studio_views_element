<?php

namespace Drupal\site_studio_views_element\Plugin\CustomElement;

use Drupal\cohesion_elements\CustomElementPluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Element allowing a view to be embedded with its settings inferred from the View.
 *
 * @CustomElement(
 *   id = "site_studio_views_element",
 *   label = @Translation("Drupal View")
 * )
 */
class ViewsElement extends CustomElementPluginBase {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $views = $this->entityTypeManager->getStorage('view')->loadMultiple();

    $displays = [];

    foreach ($views as $view) {
      foreach ($view->get('display') as $display) {
        if ($display["display_plugin"] == 'block') {
          $id = $view->id() . ':' . $display['id'];
          $label = $view->label();
          $title = $display['display_title'];
          $default_title = $this->t('Block')->__toString();
          $description = $display['display_options']['block_description'] ?? null;

          $displays[$id] = implode(' - ', array_unique(array_filter([
            $label,
            $title !== $default_title ? $title : '',
            $description,
          ]))) . ' (' . $id . ')';
        }
      }
    }

    asort($displays, SORT_NATURAL);

    return [
      'view_id' => [
        'title' => 'Drupal View',
        'type' => 'select',
        'options' => $displays,
        'required' => TRUE,
        'validationMessage' => 'This field is required.',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($element_settings, $element_markup, $element_class, $element_context = []) {
    $results = [];

    if (empty($element_settings['view_id'])) {
      return;
    }
    $data = explode(':', $element_settings['view_id']);
    $view_id = $data[0];
    $view_display = $data[1];

    if ($view = Views::getView($view_id)) {
      $view->setDisplay($view_display);
      $view->execute();
      $results = $view->buildRenderable($view_display);
    }

    return [
      '#theme' => 'site_studio_views_element',
      '#template' => 'site-studio-views-element',
      '#elementSettings' => $element_settings,
      '#elementMarkup' => $results,
      '#elementContext' => $element_context,
      '#elementClass' => $element_class,
    ];
  }
}
